<?php

namespace Modules\Member\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Member\Events\Handlers\RegisterMemberSidebar;

class MemberServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterMemberSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            // append translations
        });
    }

    public function boot()
    {
        $this->publishConfig('member', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

        // Auth Config for Member Module
        $configAuthGuards = config('auth.guards');
        $configAuthGuards['member'] = [
            'driver' => 'session',
            'provider' => 'members',
        ];
        $configAuthProviders = config('auth.providers');
        $configAuthProviders['members'] = [
            'driver' => 'eloquent',
            'model' => Modules\Member\Entities\Member::class,
        ];
        $configAuthPasswords = config('auth.passwords');
        $configAuthPasswords['members'] = [
            'provider' => 'members',
            'email' => 'auth.emails.password',
            'table' => 'password_resets',
            'expire' => 60,
        ];
        // Add Auth Config
        $addedAuthConfig = [
            'auth.guards' => $configAuthGuards,
            'auth.providers' => $configAuthProviders,
            'auth.passwords' => $configAuthPasswords,   
        ];
        config($addedAuthConfig);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
// add bindings
    }
}
